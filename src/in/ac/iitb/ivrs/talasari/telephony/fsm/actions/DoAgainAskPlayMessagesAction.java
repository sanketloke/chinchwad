package in.ac.iitb.ivrs.talasari.telephony.fsm.actions;

import in.ac.iitb.ivrs.talasari.config.Configs;
import in.ac.iitb.ivrs.talasari.model.EntityManagerService;
import in.ac.iitb.ivrs.talasari.model.entities.Message;
import in.ac.iitb.ivrs.telephony.base.IVRSession;

import java.util.List;

import javax.persistence.EntityManager;

import com.continuent.tungsten.commons.patterns.fsm.Action;
import com.continuent.tungsten.commons.patterns.fsm.Event;
import com.continuent.tungsten.commons.patterns.fsm.Transition;
import com.continuent.tungsten.commons.patterns.fsm.TransitionFailureException;
import com.continuent.tungsten.commons.patterns.fsm.TransitionRollbackException;
import com.ozonetel.kookoo.CollectDtmf;
import com.ozonetel.kookoo.Response;

public class DoAgainAskPlayMessagesAction implements Action<IVRSession> {

	@Override
	public void doAction(Event<?> event, IVRSession session, Transition<IVRSession, ?> transition, int actionType)
			throws TransitionRollbackException, TransitionFailureException {

		Response response = session.getResponse();
		EntityManager em = EntityManagerService.getEntityManager();

		session.addAutoIncrement();
       
		CollectDtmf cd = new CollectDtmf();
		List<Message> message = em.createNamedQuery("Message.findMostRecent", Message.class).getResultList();
		int size = message.size();
		if(size > session.getInvalidTries())
		{
			Message messages = em.createNamedQuery("Message.findMostRecent", Message.class).getResultList().get(session.getAutoIncrement());

			cd.setMaxDigits(1);
			cd.setTimeOut(0);
			//cd.addPlayAudio(Configs.Voice.VOICE_DIR+"/press1ToSkipMessage.mp3");
			cd.addPlayAudio(Configs.Voice.VOICE_DIR + "/nextMessageIs.wav");
			cd.addPlayAudio(messages.getMessageUrl());

			response.addCollectDtmf(cd);
		}
		else {
			response.addPlayAudio(Configs.Voice.VOICE_DIR + "/finishedPlayingMessages.wav");

		}
	}

}
