package in.ac.iitb.ivrs.talasari.model.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the message_type database table.
 * 
 */
@Entity
@Table(name="message_type")
@NamedQuery(name="MessageType.findAll", query="SELECT m FROM MessageType m")
public class MessageType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="message_type_id")
	private int messageTypeId;

	//bi-directional many-to-one association to FixedMessageType
	@ManyToOne
	@JoinColumn(name="message_type")
	private FixedMessageType fixedMessageType;

	//bi-directional many-to-one association to Message
	@ManyToOne
	@JoinColumn(name="message_id")
	private Message message;

	//uni-directional many-to-one association to Moderator
	@ManyToOne
	@JoinColumn(name="moderator_id")
	private Moderator moderator;

	public MessageType() {
	}

	public int getMessageTypeId() {
		return this.messageTypeId;
	}

	public void setMessageTypeId(int messageTypeId) {
		this.messageTypeId = messageTypeId;
	}

	public FixedMessageType getFixedMessageType() {
		return this.fixedMessageType;
	}

	public void setFixedMessageType(FixedMessageType fixedMessageType) {
		this.fixedMessageType = fixedMessageType;
	}

	public Message getMessage() {
		return this.message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public Moderator getModerator() {
		return this.moderator;
	}

	public void setModerator(Moderator moderator) {
		this.moderator = moderator;
	}

}