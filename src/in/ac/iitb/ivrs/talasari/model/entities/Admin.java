package in.ac.iitb.ivrs.talasari.model.entities;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the admin database table.
 * 
 */
@Entity
@Table(name="admin")
@NamedQueries({
@NamedQuery(name="Admin.findAll", query="SELECT a FROM Admin a Where a.userId= :usr AND a.sha256Password= :pass")
})
public class Admin implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="user_id")
	private String userId;

	@Column(name="sha256_password")
	private String sha256Password;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="phone_no")
	private User user;

	public Admin() {
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSha256Password() {
		return this.sha256Password;
	}

	public void setSha256Password(String sha256Password) {
		this.sha256Password = sha256Password;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}