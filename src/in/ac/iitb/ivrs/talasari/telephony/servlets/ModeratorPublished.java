package in.ac.iitb.ivrs.talasari.telephony.servlets;

import in.ac.iitb.ivrs.talasari.model.EntityManagerService;
import in.ac.iitb.ivrs.talasari.model.entities.Message;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Servlet implementation class ModeratorPublished
 */
@WebServlet("/ModeratorPublished")
public class ModeratorPublished extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModeratorPublished() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		JSONArray jsonArr = new JSONArray();
		EntityManager em = EntityManagerService.getEntityManager();
		List<Message> message = em.createNamedQuery("Message.findAll", Message.class).getResultList();
		for (int i=0;i<message.size();i++){
			JSONObject jsonObj= new JSONObject();
			try {
				jsonObj.put("id", message.get(i).getMessageId());
				jsonObj.put("url", message.get(i).getMessageUrl());
				jsonObj.put("duration", message.get(i).getMessageDuration());
				jsonObj.put("published", message.get(i).getPublished());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			//System.out.println("msg "+message.get(i).getMessageId());
			jsonArr.put(jsonObj);
		}
		response.setContentType("application/json");
		response.getWriter().write(jsonArr.toString());
		
		//response.sendRedirect("index.jsp?message="+message);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
