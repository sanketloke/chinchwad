package in.ac.iitb.ivrs.talasari.telephony.fsm.guards;

import in.ac.iitb.ivrs.talasari.model.EntityManagerService;
import in.ac.iitb.ivrs.talasari.model.entities.Message;
import in.ac.iitb.ivrs.telephony.base.IVRSession;

import java.util.List;

import javax.persistence.EntityManager;

import com.continuent.tungsten.commons.patterns.fsm.Event;
import com.continuent.tungsten.commons.patterns.fsm.Guard;
import com.continuent.tungsten.commons.patterns.fsm.State;

/**
 * Guard condition to check if recorded messages exist in the system to listen to.
 */
public class OnMessagesExist implements Guard<IVRSession, Object> {

	Guard<IVRSession, Object> additionalGuard;
	boolean checkExist;

	/**
	 * Creates a new guard condition that evaluates to true when the appropriate messages existence condition is
	 * met.
	 * @param additionalGuard An additional guard that must evaluate to true.
	 * @param checkExist Whether to check for existence of messages instead of non-existence.
	 */
	public OnMessagesExist(Guard<IVRSession, Object> additionalGuard, boolean checkExist) {
		this.additionalGuard = additionalGuard;
		this.checkExist = checkExist;
	}

	private boolean flipIfNeeded(boolean value) {
		if (checkExist)
			return value;
		else
			return !value;
	}

	@Override
	public boolean accept(Event<Object> event, IVRSession session, State<?> state) {
		if (additionalGuard != null && !additionalGuard.accept(event, session, state))
			return false;

		EntityManager em = EntityManagerService.getEntityManager();
		List<Message> messages = em.createNamedQuery("Message.findAll", Message.class).setMaxResults(1).getResultList();
		if (messages == null || messages.isEmpty())
			return flipIfNeeded(false);
		else
			return flipIfNeeded(true);
	}

}
