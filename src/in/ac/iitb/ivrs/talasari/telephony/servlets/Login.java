package in.ac.iitb.ivrs.talasari.telephony.servlets;

import in.ac.iitb.ivrs.talasari.model.EntityManagerService;
import in.ac.iitb.ivrs.talasari.model.entities.Admin;

import java.io.IOException;
import java.security.MessageDigest;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Password: "+request.getParameter("pass"));
		System.out.println("Password sha256:"+sha256(request.getParameter("pass")));
		 
		 EntityManager em = EntityManagerService.getEntityManager();
		 try{
			TypedQuery<Admin> query = em.createNamedQuery("Admin.findAll", Admin.class);
			query.setParameter("usr", request.getParameter("usr"));
			query.setParameter("pass", sha256(request.getParameter("pass")).toString());
			List<Admin> result = query.getResultList();
			System.out.println("result: "+result.size());
			
			if (result.size() == 1){ 
				HttpSession session = request.getSession(false);
				session.setAttribute("UserName", request.getParameter("usr"));
				response.setContentType("text/plain");
				response.getWriter().write("1");				
			}
			else {
				response.setContentType("text/plain");
				response.getWriter().write("0");
			}	
		}
		catch(Exception e){
			System.out.println("e"+e);
		}
		 
	}
	
	public static String sha256(String base) {
	    try{
	        MessageDigest digest = MessageDigest.getInstance("SHA-256");
	        byte[] hash = digest.digest(base.getBytes("UTF-8"));
	        StringBuffer hexString = new StringBuffer();

	        for (int i = 0; i < hash.length; i++) {
	            String hex = Integer.toHexString(0xff & hash[i]);
	            if(hex.length() == 1) hexString.append('0');
	            hexString.append(hex);
	        }

	        return hexString.toString();
	    } catch(Exception ex){
	       throw new RuntimeException(ex);
	    }
	}

}
