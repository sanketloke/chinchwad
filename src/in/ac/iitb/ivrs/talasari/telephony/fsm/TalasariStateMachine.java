package in.ac.iitb.ivrs.talasari.telephony.fsm;

import in.ac.iitb.ivrs.talasari.config.Configs;
import in.ac.iitb.ivrs.talasari.telephony.fsm.actions.AskConfirmMessageAction;
import in.ac.iitb.ivrs.talasari.telephony.fsm.actions.AskMainMenuAction;
import in.ac.iitb.ivrs.talasari.telephony.fsm.actions.AskRecordAction;
import in.ac.iitb.ivrs.talasari.telephony.fsm.actions.DoAgainAskPlayMessagesAction;
import in.ac.iitb.ivrs.talasari.telephony.fsm.actions.DoAskPlayMessagesAction;
import in.ac.iitb.ivrs.talasari.telephony.fsm.actions.DoCancelMessageAction;
import in.ac.iitb.ivrs.talasari.telephony.fsm.actions.DoDisconnectAction;
import in.ac.iitb.ivrs.talasari.telephony.fsm.actions.DoEndAction;
import in.ac.iitb.ivrs.talasari.telephony.fsm.actions.DoInvalidInputAction;
import in.ac.iitb.ivrs.talasari.telephony.fsm.actions.DoNextAgainAskPlayMessagesAction;
import in.ac.iitb.ivrs.talasari.telephony.fsm.actions.DoReceivedRecordAction;
import in.ac.iitb.ivrs.talasari.telephony.fsm.actions.DoSaveMessageAction;
import in.ac.iitb.ivrs.talasari.telephony.fsm.actions.PlayCanceledMessageAction;
import in.ac.iitb.ivrs.talasari.telephony.fsm.actions.PlayExceededMaxTriesAction;
import in.ac.iitb.ivrs.talasari.telephony.fsm.actions.PlayIntroductionAction;
import in.ac.iitb.ivrs.talasari.telephony.fsm.actions.PlayInvalidInputAction;
import in.ac.iitb.ivrs.talasari.telephony.fsm.actions.PlayNoMessagesAction;
import in.ac.iitb.ivrs.talasari.telephony.fsm.actions.PlayRecordedMessageAction;
import in.ac.iitb.ivrs.talasari.telephony.fsm.actions.PlaySavedMessageAction;
import in.ac.iitb.ivrs.talasari.telephony.fsm.guards.OnMessagesExist;
import in.ac.iitb.ivrs.telephony.base.IVRSession;
import in.ac.iitb.ivrs.telephony.base.fsm.EventGuard;
import in.ac.iitb.ivrs.telephony.base.fsm.IVRStateTransitionMap;
import in.ac.iitb.ivrs.telephony.base.fsm.guards.OnGotDTMFKey;
import in.ac.iitb.ivrs.telephony.base.fsm.guards.OnInvalidTriesLessThanN;

import com.continuent.tungsten.commons.patterns.fsm.Action;
import com.continuent.tungsten.commons.patterns.fsm.FiniteStateException;
import com.continuent.tungsten.commons.patterns.fsm.Guard;
import com.continuent.tungsten.commons.patterns.fsm.NegationGuard;
import com.continuent.tungsten.commons.patterns.fsm.State;
import com.continuent.tungsten.commons.patterns.fsm.StateMachine;
import com.continuent.tungsten.commons.patterns.fsm.StateTransitionMap;

/**
 * Represents the state machine to manage the call flow of an IVR session. 
 */
public class TalasariStateMachine extends StateMachine<IVRSession> {

	/**
	 * The transition map that holds the rules for the state machine. Each state machine object is instantiated with
	 * this map.
	 */
	static StateTransitionMap<IVRSession> transitionMap;

	static {
		try {
			transitionMap = buildTransitionMap();
		} catch (FiniteStateException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Creates a new telephony state machine for a given session.
	 * @param session The IVR session to manage with this state machine.
	 * @throws FiniteStateException
	 */
	public TalasariStateMachine(IVRSession session) throws FiniteStateException {
		super(transitionMap, session);
	}

	/**
	 * Builds the state transition map for the state machine.
	 * This is where all the call flow logic (rules for transition between states) must go!
	 * @return The state transition map
	 * @throws FiniteStateException
	 */
	static StateTransitionMap<IVRSession> buildTransitionMap() throws FiniteStateException {
		IVRStateTransitionMap map = new IVRStateTransitionMap();

		/* ACTIONS */

		// asking user for action
		Action<IVRSession> askConfirmMessageAction = new AskConfirmMessageAction();
		Action<IVRSession> askMainMenuAction = new AskMainMenuAction();
		Action<IVRSession> askRecordAction = new AskRecordAction();

		// actions that do something with the system
		Action<IVRSession> doAskPlayMessagesAction = new DoAskPlayMessagesAction();
		Action<IVRSession> doCancelMessageAction = new DoCancelMessageAction();
		Action<IVRSession> doDisconnectAction = new DoDisconnectAction();
		Action<IVRSession> doEndAction = new DoEndAction();
		Action<IVRSession> doInvalidInputAction = new DoInvalidInputAction();
		Action<IVRSession> doReceivedRecordAction = new DoReceivedRecordAction();
		Action<IVRSession> doSaveMessageAction = new DoSaveMessageAction();
		Action<IVRSession> doAgainAskPlayMessagesAction = new DoAgainAskPlayMessagesAction();
		Action<IVRSession> doNextAgainAskPlayMessagesAction = new DoNextAgainAskPlayMessagesAction();
		
		// playing a message to the user
		Action<IVRSession> playCanceledMessageAction = new PlayCanceledMessageAction();
		Action<IVRSession> playExceededMaxTriesAction = new PlayExceededMaxTriesAction();
		Action<IVRSession> playIntroductionAction = new PlayIntroductionAction();
		Action<IVRSession> playInvalidInputAction = new PlayInvalidInputAction();
		Action<IVRSession> playNoMessagesAction = new PlayNoMessagesAction();
		Action<IVRSession> playRecordedMessageAction = new PlayRecordedMessageAction();
		Action<IVRSession> playSavedMessageAction = new PlaySavedMessageAction();

		/* STATES */

		// start state
		State<IVRSession> start = map.addStartState("Start", null);

		// end state
		State<IVRSession> end = map.addEndState("End", doEndAction);

		// parent state for the entire call flow
		State<IVRSession> callFlow = map.addActiveState("CallFlow", null, null);

		// state to initiate disconnection
		State<IVRSession> disconnect = map.addActiveState("Disconnect", callFlow, doDisconnectAction);

		// main menu states
		State<IVRSession> mainMenu = map.addActiveState("MainMenu", callFlow, askMainMenuAction);
		State<IVRSession> invalidInputMainMenu = map.addActiveState("InvalidInputMainMenu", callFlow, doInvalidInputAction);

		// record states
		State<IVRSession> askRecord = map.addActiveState("AskRecord", callFlow, askRecordAction);
		State<IVRSession> replayRecord = map.addActiveState("ReplayRecord", callFlow, playRecordedMessageAction);
		State<IVRSession> confirmRecord = map.addActiveState("ConfirmRecord", callFlow, askConfirmMessageAction);
		State<IVRSession> acceptRecord = map.addActiveState("AcceptRecord", callFlow, doSaveMessageAction, playSavedMessageAction);
		State<IVRSession> rejectRecord = map.addActiveState("RejectRecord", callFlow, doCancelMessageAction, playCanceledMessageAction);
		State<IVRSession> invalidInputConfirmRecord = map.addActiveState("InvalidInputConfirmRecord", callFlow, doInvalidInputAction);
        State<IVRSession> invalidInputLatestNextMessage=map.addActiveState("InvalidInputLatestNextMessage", callFlow, doInvalidInputAction);
		
		// listen states
		State<IVRSession> listenMessages = map.addActiveState("ListenMessages", callFlow, doAskPlayMessagesAction);
		State<IVRSession> noMessages = map.addActiveState("NoMessages", callFlow, playNoMessagesAction);
		State<IVRSession> nextListenMessages = map.addActiveState("NextListenMessages", callFlow, doAgainAskPlayMessagesAction);
		State<IVRSession> nextLatestListenMessages = map.addActiveState("AgainNextListenMessages", callFlow, doNextAgainAskPlayMessagesAction);
		State<IVRSession> invalidInputNextMessage = map.addActiveState("InvalidInputNextMessage", callFlow, doInvalidInputAction);
		
		/* CUSTOM GUARD CONDITIONS */

		Guard<IVRSession, Object> onGotDTMFKeyNot1nor2 = new OnGotDTMFKey(new String[] {"1", "2"}, false);
		Guard<IVRSession, Object> onInvalidTriesLessThan4 = new OnInvalidTriesLessThanN(Configs.Telephony.MAX_INVALID_ATTEMPTS);
		Guard<IVRSession, Object> onInvalidTriesGreaterOrEqual4 = new NegationGuard<IVRSession, Object>(onInvalidTriesLessThan4);
		Guard<IVRSession, Object> onDTMF2MessagesExist = new OnMessagesExist(EventGuard.onGotDTMFKey[2], true);
		Guard<IVRSession, Object> onDTMF2MessagesNotExist = new OnMessagesExist(EventGuard.onGotDTMFKey[2], false);
		Guard<IVRSession, Object> onGotDTMFKeyNot9 = new OnGotDTMFKey(new String[] {"9"}, false);
		Guard<IVRSession,Object> onGotDTMFKey9 = new OnGotDTMFKey(new String[] {"9"}, true);
		
		Guard<IVRSession, Object> onGotDTMFKeyNot1nor9 = new OnGotDTMFKey(new String[] {"9" , "1"}, false);
		Guard<IVRSession,Object> onGotDTMFKey1 = new OnGotDTMFKey(new String[] {"1"}, true);

		/* TRANSITIONS */

		// transitions from start
		map.allowTransition(start, EventGuard.onNewCall, mainMenu, playIntroductionAction);

		// transitions from main menu
		/*map.allowTransition(mainMenu, EventGuard.onGotDTMFKey[1], askRecord, null);
		map.allowTransition(mainMenu, onDTMF2MessagesExist, listenMessages, null);
		map.allowTransition(mainMenu, onDTMF2MessagesNotExist, noMessages, null);
		map.allowTransition(mainMenu, onGotDTMFKeyNot1nor2, invalidInputMainMenu, null);*/
		
		
		map.allowTransition(mainMenu, EventGuard.onGotDTMFKey[1], listenMessages, null);
		map.allowTransition(mainMenu, onDTMF2MessagesExist, askRecord, null);
		map.allowTransition(mainMenu, onDTMF2MessagesNotExist, noMessages, null);
		map.allowTransition(mainMenu, onGotDTMFKeyNot1nor2, invalidInputMainMenu, null);

		// transitions from invalid input at main menu
		map.allowTransition(invalidInputMainMenu, onInvalidTriesLessThan4, mainMenu, playInvalidInputAction);
		map.allowTransition(invalidInputMainMenu, onInvalidTriesGreaterOrEqual4, disconnect, playExceededMaxTriesAction);

		// transitions from ask record
		map.allowTransition(askRecord, EventGuard.onRecord, replayRecord, doReceivedRecordAction);

		// transitions from replay record
		map.allowTransition(replayRecord, EventGuard.proceed, confirmRecord, null);

		// transitions from confirm record
		map.allowTransition(confirmRecord, EventGuard.onGotDTMFKey[1], acceptRecord, null);
		map.allowTransition(confirmRecord, EventGuard.onGotDTMFKey[2], rejectRecord, null);
		map.allowTransition(confirmRecord, onGotDTMFKeyNot1nor2, invalidInputConfirmRecord, null);

		// transitions from accept record
		map.allowTransition(acceptRecord, EventGuard.proceed, mainMenu, null);

		// transitions from reject record
		map.allowTransition(rejectRecord, EventGuard.proceed, mainMenu, null);

		// transitions from invalid input at confirm record
		map.allowTransition(invalidInputConfirmRecord, onInvalidTriesLessThan4, confirmRecord, playInvalidInputAction);
		map.allowTransition(invalidInputConfirmRecord, onInvalidTriesGreaterOrEqual4, acceptRecord, null);

		// transitions from listen messages
	//	map.allowTransition(listenMessages, EventGuard.onGotDTMF, mainMenu, null);
		map.allowTransition(listenMessages, onGotDTMFKey9, mainMenu, null);

		map.allowTransition(listenMessages, onGotDTMFKeyNot9, nextListenMessages, null);
		
		map.allowTransition(nextListenMessages, onGotDTMFKey1, nextLatestListenMessages, null);
		map.allowTransition(nextListenMessages, onGotDTMFKey9, mainMenu, null);
		map.allowTransition(nextListenMessages, onGotDTMFKeyNot1nor9,invalidInputNextMessage, null);
	
		
		// transitions from invalid input at Next ListenMessage
		map.allowTransition(invalidInputNextMessage, onInvalidTriesLessThan4, nextListenMessages, playInvalidInputAction);
		map.allowTransition(invalidInputNextMessage, onInvalidTriesGreaterOrEqual4, disconnect, playExceededMaxTriesAction);
		
		
		map.allowTransition(nextLatestListenMessages,onGotDTMFKey1, nextListenMessages, null);
		map.allowTransition(nextLatestListenMessages, onGotDTMFKey9, mainMenu, null);
		map.allowTransition(nextLatestListenMessages, onGotDTMFKeyNot1nor9,invalidInputLatestNextMessage,null);
		
		
		// transitions from invalid input at Next ListenMessage
		map.allowTransition(invalidInputLatestNextMessage, onInvalidTriesLessThan4, nextLatestListenMessages, playInvalidInputAction);
	    map.allowTransition(invalidInputLatestNextMessage, onInvalidTriesGreaterOrEqual4, disconnect, playExceededMaxTriesAction);
				
		// transitions from no messages
		map.allowTransition(noMessages, EventGuard.proceed, mainMenu, null);

		// transitions from call flow
		map.allowTransition(callFlow, EventGuard.onDisconnect, end, null);

		/* Build this map */
		map.build();
		return map;
	}

}
